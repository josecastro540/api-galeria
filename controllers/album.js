'use strict'

const Album = require('../models/album.js');
let fs = require('fs');
let path = require('path');
let moment = require('moment');

function getAlbums(req, res) {
	Album.find({}, (err, albums) => {
		if (err) {
			res.status(500).send({ message: "Error en la petición" })
		} else {
			if (!albums) {
				res.status(404).send({ message: "No Existen Albums" })
			} else {
				res.status(202).send({ albums: albums });
			}
		}
	});
}


function getAlbum(req, res) {
	let albumId = req.params.id;

	Album.findById(albumId, (err, album) => {
		if (err) {
			res.status(500).send({ message: err })
		} else {
			if (!album) {
				res.status(404).send({ message: "No Existe el Album" })
			} else {
				res.status(202).send({ album: album });
			}
		}
	});
}


function saveAlbum(req, res) {
	let album = new Album();
	let params = req.body;
	album.titulo = params.titulo;
	album.descripcion = params.descripcion;
	album.fecha = params.fecha;
	let img;
	let fileToSave;

	if(req.files){
		img = req.files.portada ;
		// console.log('aca',img)
		let ext = img.name.split('.');
		let extention = ext.pop();
		if (Math.round(img.data.length / 1000) > 2000) {
			return res.status(200).send({ message: "La imagen no puede pesar maś de 2Mb" });
		}

		if (extention != 'jpg' && extention != 'png') {
			return res.status(200).send({ message: "La extensión debe ser jpg o png" });
		}

		let day = new Date()
		let nameImg = `${moment(day).unix()}-cover.${extention}`;
		let saveTo = path.join(__dirname, '../public/covers/');
		fileToSave = saveTo + nameImg;

		album.portada = nameImg;
	}



	album.save((err, albumStorage) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!albumStorage) {
				res.status(404).send({ message: 'No se ha guardado el album' });
			} else {
				if(req.files){
					img.mv(fileToSave, (err) =>{
					if (err) {
						return res.status(500).send(err);
					} else {
						res.status(200).send({ album: albumStorage });
					}
							// res.status(200).send({ album: albumStorage });
					})
				}else{
					res.status(200).send({ album: albumStorage });
				}
				

			}
		}
	});
}

function editAlbum(req, res) {

	let albumId = req.params.id;
	let update = req.body;
	let img;
	let fileToSave;


	if(req.files){
		img = req.files.portada ;
		// console.log('aca',img)
		let ext = img.name.split('.');
		let extention = ext.pop();
		if (Math.round(img.data.length / 1000) > 2000) {
			return res.status(200).send({ message: "La imagen no puede pesar maś de 2Mb" });
		}

		if (extention != 'jpg' && extention != 'png') {
			return res.status(200).send({ message: "La extensión debe ser jpg o png" });
		}

		let day = new Date()
		let nameImg = `${moment(day).unix()}-cover.${extention}`;
		let saveTo = path.join(__dirname, '../public/covers/');
		fileToSave = saveTo + nameImg;

		update.portada = nameImg;
	}

	

	Album.findByIdAndUpdate(albumId, update, (err, albumUpdated) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!albumUpdated) {
				res.status(404).send({ message: 'No se ha actualizado el album' });
			} else {
				if(req.files){
					img.mv(fileToSave, (err) =>{
						if (err) {
							return res.status(500).send(err);
						} else {
							res.status(200).send({ album: albumUpdated });
						}
							
					})		
				}else{
					res.status(200).send({ album: albumUpdated });
				}
						
			}
		}
	});
}

function removeAlbum(req, res) {
	let albumId = req.params.id;

	Album.findByIdAndRemove(albumId, (err, albumDeleted) => {
		if (err) {
			res.status(500).send({ message: err });
		} else {
			if (!albumDeleted) {
				res.status(404).send({ message: 'No se ha borrado el album' });
			} else {
				res.status(200).send({ album: albumDeleted });
			}
		}
	})
}

function editCover(req, res) {
	if (req.files == null || !req.body.id) {
		return res.status(200).send({ message: "No se ha adjuntado ninguna imagen o falta el id" });
	} else {
		let img = req.files.portada;
		let update = req.body;
		let ext = img.name.split('.');
		let extention = ext.pop();
		if (Math.round(img.data.length / 1000) > 2000) {
			return res.status(200).send({ message: "La imagen no puede pesar maś de 2Mb" });
		}
		if (extention != 'jpg' && extention != 'png') {
			return res.status(200).send({ message: "La extensión debe ser jpg o png" });
		}
		let day = new Date()
		let nameImg = `${moment(day).unix()}-cover.${extention}`;
		let saveTo = path.join(__dirname, '../public/covers/');
		let fileToSave = saveTo + nameImg;

		img.mv(fileToSave, (err) => {
			if (err) {
				return res.status(500).send(err);
			} else {
				update.portada = nameImg;
				Album.findByIdAndUpdate(req.body.id, update, (err, coverUpdate) => {
					// return response.sendDataResponse(message.ok_msg, message.ok_code, iconUpdate, (iconUpdate) => {
					res.status(200).send('Album actualizado');
					// })
				})
			}
		})

	}	
}

function showCover(req, res) {
	let albumCover = req.params.albumCover;
	let saveTo = path.join(__dirname, '../public/covers/');
	fs.exists(saveTo + albumCover, (exists) => {
		if (exists) {
			res.sendFile(saveTo + albumCover);
		} else {
				res.status(200).send({message: 'No existe el archivo'});
		}
	})
}



module.exports = {
	getAlbums,
	getAlbum,
	saveAlbum,
	editAlbum,
	removeAlbum,
	editCover,
	showCover
};

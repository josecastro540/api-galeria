'use strict'

let mongoose = require('mongoose');
let port = process.env.PORT || 3700;
let app = require('./app.js');


mongoose.connect('mongodb://root:123456@ds125565.mlab.com:25565/api-galeria', (err,res)=>{
	if(err){
		throw err;
	}else{
		console.log("Conectado a la base de datos");

		app.listen(port, ()=>{
			console.log(`API RESTful de app escuchando en el puerto ${port}`)
		});
	}
});




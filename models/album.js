'use strict'

let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let AlbumSchema = new Schema({
	titulo: {type: String, required: "El nombre es requerido"},
	descripcion:{type: String},
	fecha: {
		type: Date, 
		default: function(){
	        return Date.now();
	    }
	},
	portada: {
		type:String, 
		default: null
	}
})

module.exports = mongoose.model('Album', AlbumSchema);
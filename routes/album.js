'use strict'

let express = require('express');
let albumController = require('../controllers/album.js');
let api = express.Router();

api.get('/albums', albumController.getAlbums);
api.get('/album/:id', albumController.getAlbum);
api.post('/create-album', albumController.saveAlbum);
api.get('/show-cover/:albumCover', albumController.showCover);
api.post('/edit-cover', albumController.editCover);
api.put('/edit-album/:id', albumController.editAlbum);
api.delete('/delete-album/:id', albumController.removeAlbum);



module.exports = api;


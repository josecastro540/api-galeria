'use strict'

let express = require('express');
let fileUpload = require('express-fileupload');
let bodyParser = require('body-parser');
let app = express();

//carga de rutas
let albumRoutes = require('./routes/album.js')

//Body Parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(fileUpload());
app.use(express.static('public'));


//CORS
app.use((req, res, next) =>{
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization');
	res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
	res.header('Allow','GET, POST, OPTIONS, PUT, DELETE');
	next();
});



//rutas base
app.use('/api', albumRoutes);


module.exports = app;
